package cn.enjoy.controller;

import cn.enjoy.service.IProductService;
import cn.enjoy.vo.BaseResponse;
import cn.enjoy.vo.Product;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
@RestController
@RequestMapping("/product")
public class ProductController {

    @Resource
    private DiscoveryClient client ; // 进行Eureka的发现服务

    @Resource
    private IProductService iProductService;

    /**
     * 本系统中我们直接在这里声明参数（临时方案）
     * 在开放业务只在这里定义了
     * ystrixCommand 命令的key值，默认值为注解方法的名称
     * commandKey = "registArea",
     * HystrixCommand 命令所属的组的名称：默认注解方法类的名称
     * groupKey = "registAreaGroup",
     * 线程池名称，默认定义为groupKey
     * threadPoolKey = "registAreaGroup"
     * 然后在配置文件中设置了默认的参数，
     * 也根据不同的接口设置了不同的超时时间。
     * @param id
     * @return
     */
    @RequestMapping(value="/get/{id}")
    @HystrixCommand(fallbackMethod = "getFallback",commandProperties = {
            @HystrixProperty(name = "circuitBreaker.enabled", value = "true"),//是否开启断路器
            @HystrixProperty(name = "circuitBreaker.requestVolumeThreshold", value = "10"),// 请求次数
            @HystrixProperty(name = "circuitBreaker.sleepWindowInMilliseconds", value = "10000"), //时间窗口期
            @HystrixProperty(name = "circuitBreaker.errorThresholdPercentage", value = "60") //失败率达到多少后跳闸
    })
    public Object get(@PathVariable("id") long id) {

        System.out.println("进入正常代码块+++++++++++++++++++");

        //休眠1.5秒产生熔断
        try {
            Thread.sleep(15000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        Product product = this.iProductService.get(id);
        if(product==null){
            throw new RuntimeException("aa");
        }
        return  product;
    }


    //降级方法
    public Object getFallback(@PathVariable("id") long id) {
        System.out.println("触发降级代码块---------------------");

        Product product = new Product();
        product.setProductName("HystrixName");
        product.setProductDesc("HystrixDesc");
        product.setProductId(0L);
        return product;
    }



    @RequestMapping(value="/add")
    public BaseResponse add(@RequestBody Product product) {
        boolean add = this.iProductService.add(product);
        if(add){
            return BaseResponse.success("成功");
        }else{
            return BaseResponse.fail("失败");
        }
    }

    @RequestMapping(value="/update",method = RequestMethod.POST)
    public Object update(Product product) {
        return this.iProductService.update(product) ;
    }

    @RequestMapping(value="/update2",method = RequestMethod.POST)
    public Object update2(Product product) {
        return this.iProductService.update2(product) ;
    }


    @RequestMapping(value="/list")
    public Object list() {
        return this.iProductService.list() ;
    }

    @RequestMapping("/discover")
    public Object discover() { // 直接返回发现服务信息
        return this.client ;
    }
}