package cn.enjoy.service;

import cn.enjoy.feign.FeignClientConfig;
import cn.enjoy.service.fallback.IProductClientServiceFallbackFactory;
import cn.enjoy.vo.BaseResponse;
import cn.enjoy.vo.Product;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;
//定义feign发起rest请求的配置类，定义熔断器处理失败时的降级类 ,configuration = FeignClientConfig.class,fallbackFactory = IProductClientServiceFallbackFactory.class
@FeignClient(name = "microcloud-config-product-client",configuration = FeignClientConfig.class,fallbackFactory = IProductClientServiceFallbackFactory.class)
public interface IProductClientService {
    @RequestMapping("/product/get/{id}")
    public Product getProduct(@PathVariable("id") long id);

    @RequestMapping("/product/list")
    public  List<Product> listProduct() ;

    @RequestMapping("/product/add")
    public BaseResponse add(@RequestBody Product product);


    }