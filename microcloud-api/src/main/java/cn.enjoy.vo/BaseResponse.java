
package cn.enjoy.vo;

import java.io.Serializable;

/**
 * 基本响应对象
 */
public class BaseResponse  implements Serializable {
    /**
     * 成功状态码
     */
    public static final String SUCCESS = "0";
    /**
     * 失败状态码
     */
    public static final String FAIL = "1";
    /**
     * 状态码
     */
    private String status;
    /**
     * 消息
     */
    private String message;
    /**
     * 结果
     */
    private Object result;

    public BaseResponse() {
    }

    public BaseResponse(String status, String message, Object result) {
        this.status = status;
        this.message = message;
        this.result = result;
    }

    /**
     * 操作成功
     */
    public static BaseResponse success() {
        return success(null);
    }

    /**
     * 操作成功，返回数据
     *
     * @param result 结果
     */
    public static BaseResponse success(Object result) {
        return new BaseResponse(SUCCESS, null, result);
    }
    public static BaseResponse success(String message ,Object result) {
        return new BaseResponse(SUCCESS, message, result);
    }

    /**
     * 操作失败，返回错误消息
     *
     * @param message 错误消息
     */
    public static BaseResponse fail(String message) {
        return new BaseResponse(FAIL, message, null);
    }

    /**
     * 操作失败，返回错误集合
     *
     * @param result 错误消息集合
     */
    public static BaseResponse fail(String message, Object result) {
        return new BaseResponse(FAIL, message, result);
    }

    /**
     * 操作失败，返回状态码
     *
     * @param status 状态码
     */
    public static BaseResponse status(String status) {
        return new BaseResponse(status, null, null);
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getResult() {
        return result;
    }

    public void setResult(Object result) {
        this.result = result;
    }
}
