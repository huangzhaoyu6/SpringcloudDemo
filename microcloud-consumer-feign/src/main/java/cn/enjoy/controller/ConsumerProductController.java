package cn.enjoy.controller;


import cn.enjoy.service.IProductClientService;
import cn.enjoy.service.IZUUlClientService;
import cn.enjoy.vo.BaseResponse;
import cn.enjoy.vo.Product;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping("/consumer")
public class ConsumerProductController {

    @Resource
    private IProductClientService iProductClientService;
//    private IZUUlClientService iProductClientService;

    @RequestMapping("/product/get/{id}")
    public Object getProduct(@PathVariable("id") long id) {
        return iProductClientService.getProduct(id);
    }
    @RequestMapping("/hello")
    public String getProducthe(long id) {
        return "aaa";
    }

    @RequestMapping("/product/list")
    public  Object listProduct() {
        return iProductClientService.listProduct();
    }

    @RequestMapping("/product/add")
    public BaseResponse addPorduct(Product product) {
        BaseResponse add = iProductClientService.add(product);
        return add;
    }
}